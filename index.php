<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.

-->

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test of the jQuery slideToggle function</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    </head>



    <style>

        .family-details {
            text-align: center;        

        }
        .family-name{    

            background-color: #FFA;


        }
        .first-name{
            background-color:#eeeeee;

        }

        .picture_plus {
            width:20px; 
            height:20px;
        }


        div.first-name {
            margin: 5px;
            padding: 5px;
            border: 1px solid #0000ff;
            height: auto;
            width: auto;
            float: left;
            text-align: center;
        }
        div.family-name {
            margin: 5px;
            padding: 5px;
            border: 1px solid #0000ff;
            height: auto;
            width: auto;
            float: left;
            text-align: center;
        }


        .family-details {
            display: table;
        }

        .row  {
            display: table-row;
        }

        .rowbig  {
            display: table-row;
        }

        .family-name, .first-name, .picture_plus  {
            display: table-cell;
        }


    </style>

    <body>
        <h1>Test of the jQuery slideToggle function</h1>

        <p>This example shows how you can do real time hide/show in the table using 
            JavaScript and Php (Ajax) interactions.</p>

        <form name="readdata" action="index.php" method="POST">
            <input type="submit" value="process" name="process" />
        </form>
        <br>
        <br>
        <br>
        <?php
// put your code here

        $data = array();

        if ($_POST['process'] == "process") {
            $datafile = file("marfin_family.txt") or die("Sorry, Couldn't load data!!");
        }
        ?>   


        <?php foreach ($datafile as $data) : ?>
            <?php list($name, $famname) = explode(" ", $data); ?>

            <div class="family-details">
                <div class="rowbig">  
                    <div class="picture_plus" >-</div>   
                    
                    <div class="row">                                          


                        <div class="first-name">  <?php echo $name ?>  </div>

                        <div class="family-name">  <?php echo $famname ?> </div>
                        <div style="clear:both;"></div>    

                    </div>
                </div>
                <div style="clear:both;"></div>                     
            </div>
        <?php endforeach ?>



        <script>
            $(document).ready(function () {
                $(".picture_plus").click(function () {
                    $(this).parent().find('div.row').slideToggle();
                    var text = $(this).text();
                    console.log(text);
                    if (text=="-") {
                        text = "+";
                    } else if (text=="+") {
                           text = "-";
                    }
                    $(this).html(text);
                });
            });
        </script>


    </body>
</html>
