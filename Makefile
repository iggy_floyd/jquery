# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project


all: configuration doc clean README.wiki


# to check that the system all needed components
configuration:
	@./configure



doc: README.wiki

	-@ mkdir doc
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	-@ rm *info
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc; rm -r {}" | sh


run: configuration

	- firefox  localhost:8010  &
	- php -S localhost:8010 -t ./ 


# to clean all temporary stuff
clean: 
	-@rm -r config.log autom4te.cache



.PHONY: configuration clean all doc run
